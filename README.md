# Visicut-Settings-Walkingrobot

This repository holds the laser settings for the Walkingrobot  

It is intended to get our most recent settings into [Visicut](https://github.com/t-oster/visicut).   

For a explanation see https://github.com/t-oster/VisiCut/wiki/How-to-add-default-settings-for-your-lab .      